export class Player {
    name: string;
    imageUrl: string;
    position: number;
    constructor(name: string, imgUrl: string, position: number){
        this.name = name;
        this.imageUrl = imgUrl;
        this.position = position;
    }
}
