import { Injectable } from '@angular/core';
import { TicTacToeMatch } from './tic-tac-toe-match';
import { Observable }	from	'rxjs/Observable';
import 'rxjs/add/operator/share';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class MatchService {
  private match : TicTacToeMatch;
  private	turnObservable:	Observable<number>;
  private _turnObserver: Observer<number>;
  private	winnerObservable:	Observable<string>;
  private _winnerObserver: Observer<string>;
  private restartGameSignal: Subject<boolean>;
  restartGame$: any;

  constructor() {
    this.turnObservable = new Observable(observer => 
      this._turnObserver = observer
    ).share();
    this.winnerObservable = new Observable(observer => 
      this._winnerObserver = observer
    )
    this.restartGameSignal = new Subject<boolean>();
    this.restartGame$ = this.restartGameSignal.asObservable();
  }

  buildMatch(player1: string, player2: string){
    this.match = new TicTacToeMatch(player1, "", player2, "");
    return this;
  }

  makeMove(position: number){
    if(this.match.positionFilled(position)) {
      return "";
    }
    else {
      let img = this.match.addMovement(position);
      this._turnObserver.next(this.match.turnOf);
      if(this.match.checkWinner())
        this._winnerObserver.next(this.match.result); 

      return img;
    }   
  }  

  getPlayer1(){
    return this.match.player1;
  }
  getPlayer2(){
    return this.match.player2;
  }
  getTurnObservable(){
    return this.turnObservable;
  }
  getWinnerObservable(){
    return this.winnerObservable;
  }
  restartMatch(){
    this.match.resetMatch();
    this.restartGameSignal.next(true);
    this._turnObserver.next(this.match.turnOf);
  }
  startTurns(){
    this._turnObserver.next(this.match.turnOf);
  }

}
