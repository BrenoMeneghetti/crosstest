import { Component, OnInit, Input } from '@angular/core';
import { MatchService } from '../match.service';
@Component({
  selector: 'app-grid-square',
  templateUrl: './grid-square.component.html',
  styleUrls: ['./grid-square.component.css']
})
export class GridSquareComponent implements OnInit {
  private enable: boolean;
  private imageUrl: string; 
  private matchService: MatchService;
  @Input() position: number = 0;

  constructor(matchService: MatchService) { 
    this.matchService = matchService;
    this.enable=true;
  }

  ngOnInit() {
    this.matchService.restartGame$.subscribe(
      astronaut => {
        this.enable = true;
        this.imageUrl = "";
      });
  }

  clickSquare() {
    if(this.enable){
      let img = this.matchService.makeMove(this.position);
      if(img){
        this.imageUrl = img;
        this.enable=false;
      }
    }
  }

}
