import { Component, OnInit } from '@angular/core';
import { MatchService } from '../match.service';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css'],
  providers: []
})
export class MatchComponent implements OnInit {
  private matchService: MatchService;
  private matchSet: boolean = false;
  private playerTurn: number = 0;
  private result: string = "";
  constructor(matchService: MatchService) { 
    this.matchService = matchService;
  }

  ngOnInit() {
        let subscription = this.matchService.getWinnerObservable().subscribe(
      value => this.result = value
    );
  }

  openMatch(event) {
    this.matchSet = true;
  }

  setStart(){
    this.matchService.startTurns();
  }
  restartMatch(){
    this.matchService.restartMatch();
  }
}
