import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MatchService } from '../match.service';

@Component({
  selector: 'app-match-setup',
  templateUrl: './match-setup.component.html',
  providers: [],
  styleUrls: ['./match-setup.component.css']
})
export class MatchSetupComponent implements OnInit {
  warningMessage: string = "";
  matchService: MatchService;
  @Output() matchBuilded: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(matchService: MatchService) {
    this.matchService = matchService;
  }

  ngOnInit() {
  }

  setupMatch(form) {
    this.warningMessage = "";
    if(!form.namePlayer1 || !form.namePlayer2) {
      console.log("Invalid");
      this.warningMessage = "You need two players to start the game!";
    }
    else {
      this.matchService.buildMatch(form.namePlayer1, form.namePlayer2 );
      this.matchBuilded.emit(true);
    }
  }

}
